* 2016-04-11 (75 tests total +)

| № | ФИО | Дом | Контр | Сем | *Итого* |
|---+-----+-----+-------+-----+---------|
|   |     |     |       |     |         |
|   |     |     |       |     |         |
|   |     |     |       |     |         |
|   |     |     |       |     |         |
|   |     |     |       |     |         |
|   |     |     |       |     |         |
|   |     |     |       |     |         |
|   |     |     |       |     |         |
|   |     |     |       |     |         |


* 2016-03-28 (75 tests total)

(!) Не объявляйте инстанс Show, напишите свой Show'!

|  id | name                   | errors | failures | tests                                          |
|-----+------------------------+--------+----------+------------------------------------------------|
|  27 | "TarasenkoKate"        |        |          |                                                |
|  26 | "Ilnur Yakupov"        |        |          |                                                |
|  25 | "Zagirova Elina"       |      1 |        3 | groupBy simplify simplify simplify             |
|  24 | "Rustem Khabetdinov"   |        |          | groupBy simplify simplify simplify             |
|  23 | "Rustavil"             |        |          | groupBy simplify simplify simplify             |
|  22 | "Alexander Kel"        |        |          | groupBy simplify simplify simplify             |
|  20 | "Denis Gerasimov"      |        |          | groupBy simplify simplify simplify             |
|  19 | "Ilya Tsygankov"       |        |          | groupBy simplify simplify simplify             |
|  18 | "Renata Zaripova"      |        |          | groupBy simplify simplify simplify             |
|  17 | "Aydar Farrakhov"      |        |          | groupBy simplify simplify simplify             |
|  16 | "Ayrat Khadeev"        |        |          | groupBy simplify simplify simplify             |
|   6 | "Maxim Ignatiev"       |      1 |        5 | Show' Show' groupBy simplify simplify simplify |
|  15 | "railrem"              |        |          | Show' Show' groupBy simplify simplify simplify |
|  14 | "Ruzal Yumaev"         |        |          | Show' Show' groupBy simplify simplify simplify |
|  12 | "Ildar Sayfeev"        |        |          | Show' Show' groupBy simplify simplify simplify |
|   8 | "Kirill Kayumov"       |        |          | Show' Show' groupBy simplify simplify simplify |
|   9 | "Rinat Salemgaraev"    |        |          | Show' Show' groupBy simplify simplify simplify |
|   7 | "Arthur Sagidulin"     |        |          | Show' Show' groupBy simplify simplify simplify |
|  54 | "Sasha Suhanaewa"      |        |          | Show' Show' groupBy simplify simplify simplify |
|  53 | "Minushina Rina"       |        |          | Show' Show' groupBy simplify simplify simplify |
|  52 | "Sanbka"               |        |          | Show' Show' groupBy simplify simplify simplify |
|  51 | "Berkovich Mira"       |        |          | Show' Show' groupBy simplify simplify simplify |
|  50 | "Vladislav Baimurzin"  |        |          | Show' Show' groupBy simplify simplify simplify |
|  49 | "Almaz Sabitov"        |        |          | Show' Show' groupBy simplify simplify simplify |
|  48 | "Dilyara Altynbaeva"   |        |          | Show' Show' groupBy simplify simplify simplify |
|  47 | "Rim Gazizov"          |        |          | Show' Show' groupBy simplify simplify simplify |
|  46 | "Nail Shaikhraziev"    |        |          | Show' Show' groupBy simplify simplify simplify |
|  45 | "Vladislav Boyko"      |        |          | Show' Show' groupBy simplify simplify simplify |
|  44 | "Himi Shoichi"         |        |          | Show' Show' groupBy simplify simplify simplify |
|  43 | "AdelB"                |        |          | Show' Show' groupBy simplify simplify simplify |
|  42 | "Roman Dmitriev"       |        |          | Show' Show' groupBy simplify simplify simplify |
|  41 | "Rustam Fashetdinov"   |        |          | Show' Show' groupBy simplify simplify simplify |
|  40 | "Сhristina Osipova"    |        |          | Show' Show' groupBy simplify simplify simplify |
|  39 | "Aynur"                |        |          | Show' Show' groupBy simplify simplify simplify |
|  37 | "Nurshat Nizamov"      |        |          | Show' Show' groupBy simplify simplify simplify |
|  35 | "Vadim Safin"          |        |          | Show' Show' groupBy simplify simplify simplify |
|  34 | "AnastasiyaA"          |        |          | Show' Show' groupBy simplify simplify simplify |
|  33 | "Дмитрий Молоканов"    |        |          | Show' Show' groupBy simplify simplify simplify |
|  32 | "Rifat Fatkhullin"     |        |          | Show' Show' groupBy simplify simplify simplify |
|  31 | "Eduard Mansurov"      |        |          | Show' Show' groupBy simplify simplify simplify |
|  30 | "Talkambaev Marsel"    |        |          | Show' Show' groupBy simplify simplify simplify |
|  29 | "Rinat Salemgaraev"    |      2 |        2 | groupBy simplify simplify simplify             |
|  28 | "Alina Harisova"       |        |          | groupBy simplify simplify simplify             |
|  89 | "Gena Mishin"          |        |          | groupBy simplify simplify simplify             |
|  87 | "Dinar Abdullin"       |        |          | groupBy simplify simplify simplify             |
|  86 | "Anna Pavlova"         |        |          | groupBy simplify simplify simplify             |
|  84 | "Камиля Назмутдинова"  |        |          | groupBy simplify simplify simplify             |
|  83 | "Vladimir Alexeev"     |        |          | groupBy simplify simplify simplify             |
|  56 | "Bogdan Volodarskij"   |      1 |        3 | groupBy simplify simplify simplify             |
|  82 | "Novikov Stanislav"    |        |          | groupBy simplify simplify simplify             |
|  81 | "Vitaly Abramov"       |        |          | groupBy simplify simplify simplify             |
|  79 | "Zagit Talipov"        |        |          | groupBy simplify simplify simplify             |
|  78 | "Mariya Z"             |        |          | groupBy simplify simplify simplify             |
|  77 | "Ruslan Yakbarov"      |        |          | groupBy simplify simplify simplify             |
|  66 | "gerasimova"           |        |          | groupBy simplify simplify simplify             |
|  68 | "Alsou"                |        |          | groupBy simplify simplify simplify             |
|  63 | "Tagirov Albert"       |        |          | groupBy simplify simplify simplify             |
|  62 | "Daniil Polyakh"       |        |          | groupBy simplify simplify simplify             |
|  59 | "Константин Ермолаев"  |        |          | groupBy simplify simplify simplify             |
|  58 | "Arthur Tsimmerman"    |        |          | groupBy simplify simplify simplify             |
|  55 | "ilhamkasymov"         |        |          | groupBy simplify simplify simplify             |
|  64 | "Oleggorru"            |        |          | groupBy simplify simplify simplify             |
|  67 | "Rafael Aglyamov"      |        |          | groupBy simplify simplify simplify             |
|  60 | "Руслана Рус"          |        |          | groupBy simplify simplify simplify             |
|  57 | "DianaMasalimova"      |        |          | groupBy simplify simplify simplify             |
|  92 | "Ayrat Mulyukov"       |        |          | groupBy simplify simplify simplify             |
|  91 | "Maxim"                |        |          | groupBy simplify simplify simplify             |
|  90 | "bazinko"              |        |          | groupBy simplify simplify simplify             |
|  98 | "AnastasiyaA"          |        |          | groupBy simplify simplify simplify             |
|  97 | "TarasenkoKate"        |        |          | groupBy simplify simplify simplify             |
|  94 | "Kirill Kayumov"       |        |          | groupBy simplify simplify simplify             |
|  96 | "Mariya Z"             |        |          | groupBy simplify simplify simplify             |
|  95 | "Ilnur Yakupov"        |        |          | groupBy simplify simplify simplify             |
| 104 | "Talkambaev Marsel"    |        |          | groupBy simplify simplify simplify             |
| 103 | "Almukhametova Albina" |        |          | groupBy simplify simplify simplify             |
| 101 | "Gulnaz Sharipova"     |        |          | groupBy simplify simplify simplify             |
| 105 | "Sasha Suhanaewa"      |        |          | groupBy simplify simplify simplify             |
| 111 | "Denis Chegodaev"      |        |          | groupBy simplify simplify simplify             |
| 106 | "Sasha Stepanov"       |        |          | groupBy simplify simplify simplify             |
| 110 | "Tansu Svetlikova"     |        |          | groupBy simplify simplify simplify             |
| 109 | "Nurshat Nizamov"      |        |          | groupBy simplify simplify simplify             |
| 108 | "Vladislav Boyko"      |        |          | groupBy simplify simplify simplify             |
| 107 | "Aydar Farrakhov"      |        |          | groupBy simplify simplify simplify             |
| 114 | "TarasenkoKate"        |        |          | groupBy simplify simplify simplify             |
| 113 | "Ahmetshina"           |        |          | groupBy simplify simplify simplify             |
| 112 | "Руслана Рус"          |        |          | groupBy simplify simplify simplify             |
| 124 | "Ahmetshina"           |        |          | groupBy simplify simplify simplify             |
| 120 | "Maxim"                |        |          | groupBy simplify simplify simplify             |
| 117 | "Rim Gazizov"          |        |          | groupBy simplify simplify simplify             |
| 122 | "Denis Chegodaev"      |        |          | groupBy simplify simplify simplify             |
| 121 | "Denis Chegodaev"      |        |          | groupBy simplify simplify simplify             |
| 118 | "Sasha Suhanaewa"      |        |          | groupBy simplify simplify simplify             |
| 116 | "Mariya Z"             |        |          | groupBy simplify simplify simplify             |
| 115 | "Vladislav Boyko"      |        |          | groupBy simplify simplify simplify             |
|-----+------------------------+--------+----------+------------------------------------------------|

* 2016-03-03 (31 tests total)

| PR id | Surname/Name             | Mark | Percent | Failures | Errors | Failer            |                  |         |      |                               |
|-------+--------------------------+------+---------+----------+--------+-------------------+------------------+---------+------+-------------------------------|
|    87 | Abdullin      Dinar      |      |    0.00 |     FAIL |   FAIL | CompilationFailed |                  |         |      |                               |
|    81 | Abramov       Vitaly     |      |    0.00 |     FAIL |   FAIL | CompilationFailed |                  |         |      |                               |
|    43 | AdelB         ?          |    5 |   83.33 |        2 |      0 | n!!               |                  |         |      |                               |
|    67 | Aglyamov      Rafael     |    1 |   16.67 |        6 |      0 | primeSum          | isPrimeIsNotSlow | isPrime | n!!  |                               |
|    83 | Alexeev       Vladimir   |    5 |   83.33 |        2 |      0 | n!!               |                  |         |      |                               |
| /68,/ | Alsou         ?          |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|    48 | Altynbaeva    Dilyara    |    5 |   83.33 |        2 |      0 | n!!               |                  |         |      |                               |
| /.34/ | AnastasiyaA   ?          |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|  /39/ | Aynur         ?          |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
| /50,/ | Baimurzin     Vladislav  |    4 |   66.67 |        1 |      2 | primeSum          | isPrime          |         |      |                               |
| /90,/ | bazinko       ?          |    5 |   83.33 |        2 |      0 | n!!               |                  |         |      |                               |
|    51 | Berkovich     Mira       |    4 |   66.67 |        1 |      0 | primeIsNotSlow    |                  |         |      |                               |
| /45./ | Boyko         Vladislav  |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|    93 | Chegodaev     Denis      |    5 |   83.33 |        1 |      1 | n!!               |                  |         |      |                               |
|    42 | Dmitriev      Roman      |    3 |   50.00 |        3 |      0 | primeIsNotSlow    | n!!              |         |      |                               |
|    59 | Ermolaev      Konstantin |    4 |   66.67 |        2 |      3 | primeSum          | isPrime          |         |      | Внимательно изучайте тесты!   |
| /.17/ | Farrakhov     Aydar      |    4 |   66.67 |        1 |      0 | isPrimeIsNotSlow  |                  |         |      |                               |
|  /41/ | Fashetdinov   Rustam     |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
| /32/. | Fatkhullin    Rifat      |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|    47 | Gazizov       Rim        |    1 |   16.67 |       10 |      0 | primeSum          | isPrimeIsNotSlow | isPrime | n!!  |                               |
|    20 | Gerasimov     Denis      |      |    0.00 |     FAIL |   FAIL | CompilationFailed |                  |         |      |                               |
|    66 | gerasimova    ?          |    3 |   50.00 |        0 |      5 | primeSum          | isPrime          | 1hw2    |      |                               |
|    28 | Harisova      Alina      |    5 |   83.33 |        2 |      0 | n!!               |                  |         |      |                               |
|    44 | Himi???       Shoichi??? |    4 |   66.67 |        3 |      0 | isPrime           | n!!              |         |      |                               |
|     6 | Ignatiev      Maxim      |    3 |   50.00 |        2 |      1 | isPrimeIsNotSlow  | n!!              |         |      |                               |
|    55 | Kasymov       Ilham      |      |    0.00 |     FAIL |   FAIL | CompilationFailed |                  |         |      |                               |
|   /8/ | Kayumov       Kirill     |    4 |   66.67 |        1 |      0 | isPrimeIsNotSlow  |                  |         |      |                               |
|  /22/ | Kel           Alexander  |    4 |   66.67 |        1 |      0 | isPrimeIsNotSlow  |                  |         |      |                               |
|    24 | Khabetdinov   Rustem     |    4 |   66.67 |        1 |      0 | isPrimeIsNotSlow  |                  |         |      |                               |
|    16 | Khadeev       Ayrat      |    3 |   50.00 |        1 |      2 | isPrimeIsNotSlow  | n!!              |         |      |                               |
|    21 | lvelapel      ?          |    4 |   66.67 |       -- |     -- | ?                 |                  |         |      |                               |
|    31 | Mansurov      Eduard     |    4 |   66.67 |        6 |      0 | primeSum          | isPrime          |         |      |                               |
|    78 | Mariya        Z???       |    3 |   50.00 |        2 |      1 | primeSum          | isPrimeIsNotSlow |         |      | Внимательно изучайте тесты!   |
|    57 | Masalimova    Diana      |    0 |    0.00 |       20 |      6 | primeSum          | isPrime          | n!!     | 1hw2 |                               |
|    91 | Maxim         ?          |    3 |   50.00 |        4 |      0 | isPrimeIsNotSlow  | isPrime          |         |      |                               |
|    53 | Minushina     Rina       |    4 |   66.67 |        1 |      0 | primeIsNotSlow    |                  |         |      |                               |
|    89 | Mishin        Gena       |    3 |   50.00 |        3 |      3 | primeSum          | isPrime          | 1hw2    |      |                               |
| /33/. | Molokanov     Dmitry     |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|    92 | Mulyukov      Ayrat      |    4 |   66.67 |        7 |      0 | n!!               | 1hw2             |         |      |                               |
|    84 | Nazmutdinova  Kamilya    |      |    0.00 |     FAIL |   FAIL | CompilationFailed |                  |         |      |                               |
|    37 | Nizamov       Nurshat    |    4 |   66.67 |        1 |      0 | primeIsNotSlow    |                  |         |      |                               |
|    82 | Novikov       Stanislav  |      |    0.00 |     FAIL |   FAIL | CompilationFailed |                  |         |      |                               |
|    64 | Oleggorru     ?          |    2 |   33.33 |        3 |      0 | isPrimeIsNotSlow  | isPrime          | n!!     |      |                               |
| /40./ | Osipova       Christina  |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|    86 | Pavlova       Anna       |      |    0.00 |     FAIL |   FAIL | CompilationFailed |                  |         |      |                               |
|    62 | Polyakh       Daniil     |    4 |   66.67 |        1 |      0 | isPrimeIsNotSlow  |                  |         |      |                               |
|    15 | railrem       ?          |      |    0.00 |     FAIL |   FAIL | CompilationFailed |                  |         |      |                               |
|    88 | Rus           Ruslana    |    3 |   50.00 |        3 |      3 | isPrimeIsNotSlow  | isPrime          |         |      |                               |
|    60 | Rus           Ruslana    |    0 |    0.00 |       20 |      6 | primeSum          | isPrime          | n!!     | 1hw2 |                               |
|    23 | Rustavil      ?          |    4 |   66.67 |        1 |      0 | isPrimeIsNotSlow  |                  |         |      |                               |
| /49,/ | Sabitov       Almaz      |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|    35 | Safin         Vadim      |    3 |   50.00 |        5 |      0 | primeSum          | isPrime          | 1hw2    |      |                               |
|     7 | Sagidulin     Arthur     |    0 |    0.00 |       17 |      5 | primeSum          | isPrime          | n!!     | 1hw2 |                               |
|     9 | Salemgaraev   Rinat      |      |    0.00 |     FAIL |   FAIL | CompilationFailed |                  |         |      |                               |
|    29 | Salemgaraev   Rinat      |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|    52 | Sanbka        ?          |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|    12 | Sayfeev       Ildar      |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|    46 | Shaikhraziev  Nail       |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|    38 | Stepanov      Sasha      |    0 |    0.00 |       20 |      6 | primeSum          | isPrime          | n!!     | 1hw2 |                               |
| /54./ | Suhanaewa     Sasha      |    0 |    0.00 |       20 |      6 | primeSum          | isPrime          | n!!     | 1hw2 |                               |
|    63 | Tagirov       Albert     |      |    0.00 |     FAIL |   FAIL | CompilationFailed |                  |         |      |                               |
|    79 | Talipov       Zagit      |    2 |   33.33 |        1 |      4 | isPrimeIsNotSlow  | isPrime          | 1hw2    |      | Не изменяйте название модуля! |
|  /30/ | Talkambaev    Marsel     |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
|    27 | Tarasenko     Kate       |    4 |   66.67 |        1 |      0 | isPrimeIsNotSlow  |                  |         |      |                               |
|    58 | Tsimmerman    Arthur     |    3 |   50.00 |        3 |      0 | isPrimeIsNotSlow  |                  |         |      |                               |
|    19 | Tsygankov     Ilya       |    5 |   83.33 |        2 |      0 | n!!               |                  |         |      |                               |
|    56 | Volodarskij   Bogdan     |    2 |   33.33 |        1 |      3 | isPrimeIsNotSlow  | n!!              | 1hw2    |      |                               |
| /77,/ | Yakbarov      Ruslan     |    6 |  100.00 |        0 |      0 |                   |                  |         |      |                               |
| /.26/ | Yakupov       Ilnur      |    4 |   66.67 |        1 |      0 | isPrimeIsNotSlow  |                  |         |      |                               |
|    14 | Yumaev        Ruzal      |      |    0.00 |       -- |     -- | AccessDenied      |                  |         |      |                               |
|    25 | Zagirova      Elina      |    4 |   66.67 |        1 |      0 | isPrimeIsNotSlow  |                  |         |      |                               |
|    18 | Zaripova      Renata     |    4 |   66.67 |        1 |      0 | isPrimeIsNotSlow  |                  |         |      |                               |
#+TBLFM: $4=100*$3/6;%.2f

