{-==== ФАМИЛИЯ ИМЯ, НОМЕР ГРУППЫ ====-}

module Trouble
       ( rocket
       , Load(..)
       , Spaceship(..)
       , orbiters
       , Phrase(..)
       , finalFrontier
       ) where

import Data.Function
import Data.List

{- Дан список ступеней ракеты, каждая ступень описана
парой «тяга - масса»

   Необходимо оставить в списке только одну ступень, с
максимальным отношением тяги к массе, если масса этой
ступени равна минимальной, иначе вернуть исходный список.

λ> rocket [(120,2),(10,2),(60,14),(90,30),(5,2),(10000,9999)]
[(120,2)]
λ> rocket [(120,20),(10,2),(60,14),(90,30),(5,2),(10000,9999)]
[(120,20),(10,2),(60,14),(90,30),(5,2),(10000,9999)]

   Все деления производить нацело.
 -}
isMinM :: (Integer, Integer) -> [(Integer, Integer)] -> Bool	
isMinM (a, b) ((x, y): []) | b > y = False
			   | otherwise = True
isMinM (a, b) ((x, y):xs) | b > y = False
			  | otherwise = isMinM (a,b) xs

maxT :: [(Integer, Integer)] -> (Integer, Integer)
maxT ((x,y):[]) = (x, y) 
maxT ((x,y):((a,b):xs)) | (x `div` y) > (a `div` b) = maxT ((x,y):xs) 
			| (x `div` y) == (a `div` b) && y <= b = maxT ((x,y):xs)
			| (x `div` y) == (a `div` b) && y > b = maxT ((a,b):xs)
		        | otherwise = maxT ((a,b):xs)

rocket :: [(Integer, Integer)] -> [(Integer, Integer)]
rocket [] = []
rocket [(x, y)] = [(x,y)]
rocket x | isMinM (maxT x) x = [maxT x]
	 | otherwise = x

{- Космический корабль (КК) может состоять из:
 * ракеты, запускающей на заданную высоту несколько КК
 * полезной нагрузки из нескольких:
   - орбитальных аппаратов
   - посадочных зондов
 -}
data Load a = Orbiter a         -- орбитальный аппарат
            | Probe a           -- посадочный зонд
            deriving (Eq,Show)
data Spaceship a = Rocket Int [Spaceship a] -- высота и список кораблей
                 | Cargo [Load a]           -- полезная нагрузка
                 deriving (Eq,Show)

{- Дан список КК

   Вернуть список всех орбитальных аппаратов

λ> orbiters [Rocket 300 [Rocket 200 [Cargo [Orbiter "LRO", Probe "Lunokhod"]], Cargo [Orbiter "ISS"]]]
[Orbiter "ISS", Orbiter "LRO"]
 -}


orbiters :: [Spaceship a] -> [Load a]
orbiters [] = []
orbiters (x:xs) = case x of
	Rocket a (b:bs) -> orbiter b ++ orbiters bs
	Cargo c -> cargoOrb c

orbiter :: Spaceship a -> [Load a]
orbiter a = case a of 
	Rocket x y -> orbiters y
	Cargo b -> cargoOrb b

cargoOrb :: [Load a] -> [Load a]
cargoOrb [] = []
cargoOrb (x:xs) = case x of
	Orbiter a -> x : (cargoOrb xs)	
	Probe a -> cargoOrb xs 

{- Даны фразы, нужно выдать того, кто сказал её

   instance Eq - не задан!
-}

data Phrase = Warp Int           -- Kirk:  Warp X, Mr. Sulu
            | BeamUp String      -- Kirk:  Beam X up, Scotty
            | IsDead String      -- McCoy: X is dead, Jim!
            | LiveLongAndProsper -- Spock: Live long and prosper
            | Fascinating        -- Spock: Fascinating!
            deriving Show

{-
λ> finalFrontier [IsDead "He", Fascinating, Warp 3, LiveLongAndProsper]
["McCoy", "Spock", "Kirk", "Spock"]
-}
finalFrontier :: [Phrase] -> [String]
finalFrontier [] = []
finalFrontier ([Warp a]) = ["Kirk"] 
finalFrontier ([BeamUp s]) = ["Kirk"] 
finalFrontier ([IsDead s]) = ["McCoy"] 
finalFrontier ([LiveLongAndProsper]) = ["Spock"] 
finalFrontier ([Fascinating]) = ["Spock"] 

finalFrontier ((Warp a):as) = ["Kirk"] ++ finalFrontier as 
finalFrontier ((BeamUp s):as) = ["Kirk"] ++ finalFrontier as 
finalFrontier ((IsDead s):as) = ["McCoy"] ++ finalFrontier as 
finalFrontier ((LiveLongAndProsper):as) = ["Spock"] ++ finalFrontier as 
finalFrontier ((Fascinating):as) = ["Spock"] ++ finalFrontier as

