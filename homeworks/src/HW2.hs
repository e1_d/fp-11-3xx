-- Тесты чуть позже

module HW2
       ( Contact (..)
       , isKnown
       , Term (..)
       , eval
       , simplify
       ) where

data Contact = On
             | Off
             | Unknown

isKnown :: Contact -> Bool
isKnown Unknown = False
isKnown _ = True

data Term = Mult Term Term      -- умножение
          | Add Term Term       -- сложение
          | Sub Term Term       -- вычитание
          | Const Int           -- константа
	  deriving Show

eval :: Term -> Int
eval (Mult x y) = (eval x) * (eval y)
eval (Add x y) = (eval x) + (eval y)
eval (Sub x y) = (eval x) - (eval y)
eval (Const x) = x

-- Раскрыть скобки
-- Mult (Add (Const 1) (Const 2)) (Const 3) ->
-- Add (Mult (Const 1) (Const 3)) (Mult (Const 2) (Const 3))
-- (1+2)*3 -> 1*3+2*3
simplify :: Term -> Term
simplify (Mult x y) = simplifier (simplifier (Mult x y))
simplify x = simplifier x

simplifier (Const x) = Const x
simplifier (Mult x (Add y z)) = Add (simplifier (Mult (simplifier x) (simplifier y))) (simplifier (Mult (simplifier x) (simplifier z)))
simplifier (Mult (Add x y) z) = Add (simplifier (Mult (simplifier x) (simplifier z))) (simplifier (Mult (simplifier y) (simplifier z))) 	
simplifier (Mult x (Sub y z)) = Sub (simplifier(Mult (simplifier x) (simplifier y))) (simplifier (Mult (simplifier x) (simplifier z)))
simplifier (Mult (Sub x y) z) = Sub (simplifier (Mult (simplifier x) (simplifier z))) (simplifier (Mult (simplifier y) (simplifier z)))
simplifier (Add x y) = Add (simplifier x) (simplifier y)
simplifier (Mult x y) = Mult (simplifier x) (simplifier y)
simplifier (Sub x y) = Sub (simplifier x) (simplifier y)
