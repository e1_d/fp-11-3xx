module HW1
       ( hw1_1
       , hw1_2
       , fact2
       , isPrime
       , primeSum
       ) where

-- Запускать при помощи: haskellstack.org
-- stack setup - установка GHC нужной версии и т.д.
-- stack build - компиляция
-- stack test  - тесты

-- |Вычислить сумму двух аргументов
hw1_1 :: Integer -> Integer -> Integer
hw1_1 a b = a + b

-- |Вычислить сумму N членов ряда
--
--
--  N
-- ---
-- \    1
--  >  ---
-- /   k^k
-- ---
-- k=1
--
-- Использовать fromIntegral для перевода из Integer в Double
hw1_2 :: Integer -> Double
hw1_2 n | n < 1 = 0
	| otherwise = 1/(fromIntegral n ^ fromIntegral n) + hw1_2 (n-1)

-- |Вычислить двойной факториал
-- n!! = 1*3*5*...*n, если n - нечетное
-- n!! = 2*4*6*...*n, если n - четное
fact2 :: Integer -> Integer
fact2 n | n <= 1 = 1
	| otherwise = n * fact2 (n-2)

-- |Проверить заданное число на простоту
-- Использовать div для целочисленного деления
-- или mod для остатка от деления
checkPrime :: Integer -> Integer -> Double -> Bool
checkPrime a b c | (fromIntegral b) > c = True
                 | otherwise = (a `mod` b) /= 0 && checkPrime a (b+1) c

isPrime :: Integer -> Bool
isPrime p | p < 2 = False
	  | otherwise = checkPrime p 2 (sqrt (fromIntegral p))

-- |Найти сумму всех простых чисел в диапазоне [a;b]
primeSum :: Integer -> Integer -> Integer
primeSum a b | a > b || b < 2 = 0
	     | isPrime a = a + primeSum (a+1) b 
	     | otherwise = primeSum (a+1) b	
	
